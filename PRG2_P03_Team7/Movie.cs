﻿// ========================================== 
// Student Number: S10223203, S10228079
// Student Name: Chong Jerome, Ho Min Teck
// Module Group: P03
// ==========================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_P03_Team7
{
    class Movie
    {
       
        public string Title { get; set; }
        public int Duration { get; set; }
        public string Classification { get; set; }
        public DateTime OpeningDate { get; set; }
        public List<Movie> GenreList { get; set; } = new List<Movie>();
        public List<Movie> MovieList { get; set; } = new List<Movie>();
        public string Genre { get; set; }
         
        public List<Screening> ScreeningList { get; set; } = new List<Screening>();

        public Movie() { }

        public Movie(string t, int dur, string classify, DateTime opendate, string genre ) { Title = t; Duration = dur; Classification = classify; OpeningDate = opendate; Genre = genre; }

       

        public void AddGenre(string  g) { g = Genre; }

        public void AddScreening(Screening s)
        {
            ScreeningList.Add(s);
        }
        
        public override string ToString()
        {
            return Title + "\t" + Duration + "\t" + Classification + "\t" + OpeningDate + "\t" + Genre;
        }


    }
}
