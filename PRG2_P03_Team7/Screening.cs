﻿// ========================================== 
// Student Number: S10223203, S10228079
// Student Name: Chong Jerome, Ho Min Teck
// Module Group: P03
// ==========================================
//Jerome do
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_P03_Team7
{
    class Screening
    {
       
        public int ScreeningNo { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public string ScreeningType { get; set; }
        public int SeatsRemaining { get; set; }
        public Cinema Cinema { get; set; }
        public Movie Movie { get; set; }

        public Screening() { }

        public Screening(int sn , DateTime sdt , string st, Cinema cinema, Movie movie)
        {
            ScreeningNo = sn;
            ScreeningDateTime = sdt;
            ScreeningType = st;
            Cinema = cinema;
            Movie = movie;

        }

        public override string ToString()
        {
            return base.ToString() + ScreeningNo + ScreeningDateTime + ScreeningType + Cinema + Movie;
        }
    }
}
