﻿/// ========================================== 
// Student Number: S10223203, S10228079
// Student Name: Chong Jerome, Ho Min Teck
// Module Group: P03
// ==========================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PRG2_P03_Team7
{
    class Program
    {
        static int orderNumber = 1;
        static int screeningNumber = 1001;

        static void Main(string[] args)
        {
            List<Movie> movieList = new List<Movie>();
            List<Screening> ScreeningList = new List<Screening>();
            List<Cinema> cinemaList = new List<Cinema>();
            List<Order> orderList = new List<Order>();
            List<Ticket> ticketList = new List<Ticket>();
            int option = 1;
            while (option != 0)
            {
                DisplayMenu();
                int opt = MenuValidation("Enter your option: ");

                // Process user option
                if (opt == 1)
                {
                    LoadMovie(movieList);
                    ListAllMovies(movieList);
                }
                else if (opt == 2)
                {
                    LoadMovie(movieList);
                    ListMovieScreening(ScreeningList, movieList, cinemaList);
                    LoadScreening(ScreeningList, cinemaList, movieList);
                    ListScreening(ScreeningList, cinemaList, movieList);
                }
                else if (opt == 3)
                {
                    Addmoviescreening(movieList, cinemaList, ScreeningList);
                }
                else if (opt == 4)
                {
                    deletemovie(ScreeningList, movieList, cinemaList, ticketList);
                }
                else if (opt == 5)
                {
                    LoadMovie(movieList);
                    LoadScreening(ScreeningList, cinemaList, movieList);
                    Screening s1 = new Screening();
                    OrderMovieTicket(movieList, ScreeningList, cinemaList, ticketList, orderList, s1);
                }
                else if (opt == 6)
                {
                    Order o1 = new Order();
                    Screening s1 = new Screening();
                    CancelMovieTicket(orderList, ScreeningList, o1, s1, ticketList);
                }
                else if (opt <= -1 || opt >= 7)
                {
                    Console.WriteLine("Invalid Number. Please Try again");
                }
                else if (opt == 0)
                    break;
            }
        }


        // Jerome - 1) Load Movie and Cinema Data
        static void LoadMovie(List<Movie> movieList)
        {

            string[] movieread = File.ReadAllLines("Movie.csv");
            for (int i = 1; i < movieread.Length; i++)
            {
                string[] movielines = movieread[i].Split(',');
                List<string> genreList = new List<string>();
                string name = movielines[0];
                int dur = Convert.ToInt32(movielines[1]);
                string classi = movielines[3];
                DateTime od = Convert.ToDateTime(movielines[4]);
                string Genre = movielines[2];
                Movie m = new Movie(name, dur, classi, od, Genre);
                movieList.Add(m);

            }
        }

        // Jerome - 1) Load Movie and Cinema Data
        static void LoadCinema(List<Cinema> cinemaList)
        {
            string[] cinemaread = File.ReadAllLines("Cinema.csv");
            for (int i = 1; i < cinemaread.Length; i++)
            {
                string[] cinemalines = cinemaread[i].Split(',');
                string name = cinemalines[0];
                int hall = Convert.ToInt32(cinemalines[1]);
                int cap = Convert.ToInt32(cinemalines[2]);
                Cinema c = new Cinema(name, hall, cap);
                cinemaList.Add(c);
            }
        }

        // 2)  Load Screening Data
        static void LoadScreening(List<Screening> ScreeningList, List<Cinema> cinemaList, List<Movie> movieList)
        {
            // read csv
            string[] screeningData = File.ReadAllLines("Screening.csv");
            int screeningNumber = 1001;
            LoadMovie(movieList);
            LoadCinema(cinemaList);
            for (int i = 1; i < screeningData.Length; i++)
            {
                // split csv data into ScreeningData
                string[] screeningline = screeningData[i].Split(',');
                // call the searchCinema method to search for cinema name and hall
                Cinema c = SearchCinema(cinemaList, screeningline[2], Convert.ToInt32(screeningline[3]));
                // add Cinema c to CinemaList. 
                cinemaList.Add(c);
                // find the movie title from the movie list using .Find
                Movie m = movieList.Find(m => m.Title == screeningline[4]);
                Screening s1;
                // uses add screening to create new screening object s 
                m.AddScreening(s1 = new Screening(screeningNumber, Convert.ToDateTime(screeningline[0]), screeningline[1], c, m));
                // add the object to ScreeningList.
                ScreeningList.Add(s1);
                // running number
                screeningNumber++;
            }
        }

        // search for cinema name and hall   
        static Cinema SearchCinema(List<Cinema> cinemaList, string name, int hall)
        {
            foreach (Cinema c in cinemaList)
            {
                // check if cinema name and hall number in cinema class and name and hall number in LoadScreening is the same.
                if (c.Name == name && c.HallNo == hall)
                {
                    // return cinema c object
                    return c;
                }
            }
            // else, return null if it does not match.
            return null;
        }


        // search for movies 
        static Movie SearchMovie(List<Movie> movieList, string Title)
        {
            foreach (Movie m in movieList)
            {
                // check if movie title in movie class matches movie title in LoadScreening method.
                if (m.Title == Title)
                {
                    // return Movie m object
                    return m;
                }
            }
            // else return null if it does not match.
            return null;
        }

        //  Main Menu to navigate between features.
        static void DisplayMenu()
        {
            Console.WriteLine("----------- Main Menu ------------");
            Console.WriteLine("[1] List all movies");
            Console.WriteLine("[2] List movie screenings");
            Console.WriteLine("[3] Add a movie screening session");
            Console.WriteLine("[4] Delete a movie screening session ");
            Console.WriteLine("[5] Order movie ticket/s");
            Console.WriteLine("[6] Cancel movie ticket");
            Console.WriteLine("[0] Exit");
            Console.WriteLine("---------------------------------");
        }

        // 3) List all movies
        //  display the information of all movies
        static void ListAllMovies(List<Movie> movieList)
        {
            int count = 0;
            Console.WriteLine("{0,-8} {1,-25} {2,-10}  {3,-15}  {4,-24}  {5,-25}", "Index", "Title", "Duration", "Classification", "Opening Date", "Genre");
            foreach (Movie m in movieList)
            {
                count++;
                Console.WriteLine("{0}\t {1,-25} {2,-10}  {3,-15}  {4,-24}  {5,-25}", count, m.Title, m.Duration, m.Classification, m.OpeningDate, m.Genre);
            }
        }

        static void ListAllCinema(List<Cinema> cinemaList)
        {
            int count = 0;
            Console.WriteLine("{0,-8} {1,-16} {2,-10}  {3,-15}", "Index", "Name", "Hall Number", "Capacity");
            foreach (Cinema c in cinemaList)
            {
                count++;
                Console.WriteLine("{0}\t {1,-15}  {2,-12} {3,-10}", count, c.Name, c.HallNo, c.Capacity);
            }
        }

        static void ListScreening(List<Screening> ScreeningList, List<Cinema> cinemaList, List<Movie> movieList)
        {
            int count = 0;
            Console.WriteLine("{0} {1} {2}  {3} {4}", "Index\t", "Screening Date Time\t", "Screening Type\t", "Cinema Name\t Hall No Capacity\t", "Movie\t");
            foreach (Screening s in ScreeningList)
            {
                count++;
                Console.WriteLine("{0}\t {1,-25} {2,-10} {3,-25} \t{4,-10}", count, s.ScreeningDateTime, s.ScreeningType, s.Cinema, s.Movie.Title);
            }

        }

        // 4) List movie screenings
        static void ListMovieScreening(List<Screening> ScreeningList, List<Movie> movieList, List<Cinema> cinemaList)
        {
            //list all movies
            ListAllMovies(movieList);
            //prompt user to select a movie
            int t = GetInt("Please select a movie by entering movie index number: ");
            // retrieve movie object
            Movie movieSelection = new Movie();
            List<Movie> mList = movieSelection.MovieList;
            string movieDetails = "";
            foreach (Movie m in movieList)
            {
                movieDetails += m.ToString() + "\n";
            }

            // retrieve and display specific movie session
            for (int i = 26; i < ScreeningList.Count; i++)
            {
                // Phantom The Musical
                if (t == 1)
                {
                    Console.WriteLine("{0} {1}  {2}  {3}", ScreeningList[2].ScreeningDateTime, ScreeningList[2].ScreeningType, ScreeningList[2].Cinema, ScreeningList[2].Movie.Title);
                    Console.WriteLine("{0} {1}  {2}  {3}", ScreeningList[15].ScreeningDateTime, ScreeningList[15].ScreeningType, ScreeningList[15].Cinema, ScreeningList[15].Movie.Title);
                }
                // Makum 2
                else if (t == 2)
                {
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[1].ScreeningDateTime, ScreeningList[1].ScreeningType, ScreeningList[1].Cinema, ScreeningList[1].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[3].ScreeningDateTime, ScreeningList[3].ScreeningType, ScreeningList[3].Cinema, ScreeningList[3].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[14].ScreeningDateTime, ScreeningList[14].ScreeningType, ScreeningList[14].Cinema, ScreeningList[14].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[16].ScreeningDateTime, ScreeningList[16].ScreeningType, ScreeningList[16].Cinema, ScreeningList[16].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[27].ScreeningDateTime, ScreeningList[27].ScreeningType, ScreeningList[27].Cinema, ScreeningList[27].Movie.Title);
                }
                // Special Delivery
                else if (t == 3)
                {
                    Console.WriteLine("No Screening Sessions is available for Special Delivery. :()");
                }
                // Nightmare Alley
                else if (t == 4)
                {
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[4].ScreeningDateTime, ScreeningList[4].ScreeningType, ScreeningList[4].Cinema, ScreeningList[4].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[7].ScreeningDateTime, ScreeningList[7].ScreeningType, ScreeningList[7].Cinema, ScreeningList[7].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[17].ScreeningDateTime, ScreeningList[17].ScreeningType, ScreeningList[17].Cinema, ScreeningList[17].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[20].ScreeningDateTime, ScreeningList[20].ScreeningType, ScreeningList[20].Cinema, ScreeningList[20].Movie.Title);
                }
                // Scream
                else if (t == 5)
                {
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[10].ScreeningDateTime, ScreeningList[10].ScreeningType, ScreeningList[10].Cinema, ScreeningList[10].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[23].ScreeningDateTime, ScreeningList[23].ScreeningType, ScreeningList[23].Cinema, ScreeningList[23].Movie.Title);
                }
                // Enna Solla Pogiral
                else if (t == 6)
                {
                    Console.WriteLine("There are no screening session for Enna Solla Pogiral");
                }
                // The Hating Game
                else if (t == 7)
                {
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[8].ScreeningDateTime, ScreeningList[8].ScreeningType, ScreeningList[8].Cinema, ScreeningList[8].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[11].ScreeningDateTime, ScreeningList[11].ScreeningType, ScreeningList[11].Cinema, ScreeningList[11].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[19].ScreeningDateTime, ScreeningList[19].ScreeningType, ScreeningList[19].Cinema, ScreeningList[19].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[21].ScreeningDateTime, ScreeningList[21].ScreeningType, ScreeningList[21].Cinema, ScreeningList[21].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[24].ScreeningDateTime, ScreeningList[24].ScreeningType, ScreeningList[24].Cinema, ScreeningList[24].Movie.Title);
                }
                // The Policeman's Lineage 
                else if (t == 8)
                {
                    Console.WriteLine("There are no sessions available for The Policeman's Lineage");
                }
                // Sing 2
                else if (t == 9)
                {
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[0].ScreeningDateTime, ScreeningList[0].ScreeningType, ScreeningList[0].Cinema, ScreeningList[0].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[5].ScreeningDateTime, ScreeningList[5].ScreeningType, ScreeningList[5].Cinema, ScreeningList[5].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[9].ScreeningDateTime, ScreeningList[9].ScreeningType, ScreeningList[9].Cinema, ScreeningList[9].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[13].ScreeningDateTime, ScreeningList[13].ScreeningType, ScreeningList[13].Cinema, ScreeningList[13].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[18].ScreeningDateTime, ScreeningList[18].ScreeningType, ScreeningList[18].Cinema, ScreeningList[18].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[22].ScreeningDateTime, ScreeningList[22].ScreeningType, ScreeningList[22].Cinema, ScreeningList[22].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[26].ScreeningDateTime, ScreeningList[26].ScreeningType, ScreeningList[26].Cinema, ScreeningList[26].Movie.Title);
                }
                // Mulholland Drive
                else if (t == 10)
                {
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[12].ScreeningDateTime, ScreeningList[12].ScreeningType, ScreeningList[12].Cinema, ScreeningList[12].Movie.Title);
                    Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t{3}", ScreeningList[25].ScreeningDateTime, ScreeningList[25].ScreeningType, ScreeningList[25].Cinema, ScreeningList[25].Movie.Title);
                }
            }
        }

        // search for screening date
        static Screening SearchScreeningDate(List<Screening> ScreeningList, DateTime dt)
        {
            foreach (Screening s in ScreeningList)
            {
                if (s.ScreeningDateTime == dt)
                {
                    return s;
                }
            }
            return null;
        }

        // 5) Add a movie screening session
        static void Addmoviescreening(List<Movie> movieList, List<Cinema> cinemaList, List<Screening> ScreeningList)
        {
            // list all movies
            LoadMovie(movieList);
            ListAllMovies(movieList);
            // prompt user to select a movie
            int t = GetInt("Please select a movie by entering movie index number: ");
            // prompt user to enter a screening type [2D/3D]
            Console.WriteLine("Please select a Screening Type (2D / 3D)");
            string st = Console.ReadLine();
            //prompt user to enter a screening date and time
            DateTime dt = GetDateTime("Please select a screening date and time: ");
            // check if the datetime entered is after the opening date of the movie
            foreach (Movie m in movieList)
            {
                if (dt < m.OpeningDate)
                {
                    Console.WriteLine("The movie is not being screened yet.");
                }
                else
                {
                    break;
                }
            }
            //  list all cinema halls
            LoadCinema(cinemaList);
            ListAllCinema(cinemaList);
            int ch = GetInt("Please select a cinema hall: ");
            if (ch > 4)
            {
                Console.WriteLine("Please enter a cinema hall from 1 to 4.");
            }
            // cleaning time
            int ct = 30;
            // check if the cinema hall is available at the datetime
            foreach (Screening s in ScreeningList)
            {
                if (s.ScreeningDateTime > dt)
                {
                    Console.WriteLine("cinema hall available");
                }
                // check for cleaning time and if cinema hall is available. 
                else if (s.Movie.Duration > ct)
                {
                    Console.WriteLine("Cinema hall is scheduled for 30 mins of cleaning time.");
                }
            }
            for (int i = 0; i < movieList.Count; i++)
            {
                Cinema cin = SearchCinema(cinemaList, cinemaList[i].Name, cinemaList[i].HallNo);
                cinemaList.Add(cin);
                Movie mov = movieList.Find(mov => mov.Title == movieList[i].Title);
                Screening s1 = new Screening(screeningNumber, dt, st, cin, mov);
                mov.AddScreening(s1);
                screeningNumber++;
                Console.WriteLine("{0}\t {1}\t {2}\t {3}\t {4}\t {5}", s1.ScreeningNo, s1.ScreeningDateTime, s1.ScreeningType, s1.Cinema.Name, s1.Cinema.HallNo, s1.Movie.Title);
            }
            Console.WriteLine("Status of movie screening session creation :" + " Successful");
        }

        // 6) Delete a movie screening session
        public static void deletemovie(List<Screening> ScreeningList, List<Movie> movieList, List<Cinema> cinemaList, List<Ticket> ticketList)
        {
            LoadScreening(ScreeningList, cinemaList, movieList);
            ListScreening(ScreeningList, cinemaList, movieList);
            /*foreach (Ticket t in ticketList)
            {
               if ( t.SeatsRemaining == t.Cinema.Capacity)
                {
                    Console.WriteLine(t);
                }
            }*/
            // prompt to select session
            int del = GetInt("Please select a session to delete:");
            //int del = Convert.ToInt32(Console.ReadLine());
            try
            {
                ScreeningList.RemoveAt(del - 1);
                ListScreening(ScreeningList, cinemaList, movieList);
                Console.WriteLine("Deletion was successful");
            }
            catch (Exception e)
            {
                Console.WriteLine("Deletion was unsuccessful");
            }

        }


        // 7) Order movie ticket/s
        public static Order OrderMovieTicket(List<Movie> movieList, List<Screening> ScreeningList, List<Cinema> cinemaList, List<Ticket> ticketList, List<Order> orderList, Screening s1)
        {
            orderNumber = 1;
            while (orderNumber < 10)
            { orderNumber++; }
            orderNumber++;
            // This ListMovieScreening display all movies, prompts the user to select a movie as well as list all movie screenings for selected movie
            // Method reused from Feature 4. 
            ListMovieScreening(ScreeningList, movieList, cinemaList);
            // prompt user to select movie screening date
            DateTime dt = GetDateTime("Please select a movie screening: ");
            // Method to search and retreive the selected movie screening date
            SearchScreeningDate(ScreeningList, dt);
            // To find the screening date time for the date selected using the .Find 
            Screening scr = ScreeningList.Find(scr => scr.ScreeningDateTime == dt);

            // Prompt user to enter the total number of tickets customer is ordering.
            int ticketQty = GetInt("Please enter the total number of tickets to order: ");

            // check if number is less than zero and negative numbers.
            if (ticketQty <= 0)
            {
                Console.WriteLine("zero and negative numbers are not permitted. Please enter a valid number");
            }
            //  Check if number entered is more than available seats.
            foreach (Cinema c in cinemaList)
            {

                if (ticketQty > c.Capacity)
                {
                    Console.WriteLine("The number you entered is more than the available seats for the screening");
                }
            }
            // prompt user if all ticket holders meet the movie classification requirements 
            foreach (Movie m in movieList)
            {
                if (m.Classification != "G")
                {
                    Console.WriteLine("Ticket holders meet the movie classification requirements [YES/NO]");
                    string requirement = Console.ReadLine();
                    if (requirement == "No")
                    {
                        Console.WriteLine("Ticket holder did not met the movie classification requirements");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Ticket holders meet the requirements ");
                        break;
                    }
                }
            }
            // create an Order object with the status “Unpaid”
            List<Order> oList = new List<Order>();
            Order order1 = new Order(orderNumber, DateTime.Now, "Unpaid");
            order1.Status = "Unpaid";
            order1.TicketList = new List<Ticket>();
            oList.Add(order1);

            InitTicketList(ticketList);
            List<Screening> sList = new List<Screening>();


            foreach (Ticket t in ticketList)
            {
                if (t is Student)      // downcast to Student Object 
                {
                    // 9a) prompt user for a response depending on the type of ticket ordered:
                    string cn = RetrieveString("Please enter your level of study (Primary,Secondary,Tertiary)");
                    // Type casting 
                    Student s = (Student)t;
                    s.LevelOfStudy = cn;
                    // 9b) create a Ticket object (Student) with the information given
                    Ticket ti = new Student(s1, cn);
                    // 9c) add the ticket object to the ticket list of the order
                    order1.AddTicket(ti);
                    // 9d) update seats remaining for the movie screening
                    s1.SeatsRemaining = 50;
                    s1.SeatsRemaining -= ticketQty;
                    Console.WriteLine("Seats Remaining: " + s1.SeatsRemaining);
                    // 10) list amount payable
                    //s.CalculatePrice(); - null value.
                    // 11) prompt user to press any key to make payment
                    Console.WriteLine("Please press any key to make payment");
                    Console.ReadKey();
                    Console.WriteLine();
                    // 12) fill in the amount details to the new order 
                    order1.Amount = 12 * ticketQty;
                    Console.WriteLine("Amount Paid: ($)" + "\t" + order1.Amount);
                    // 13) change order status to “Paid”
                    order1.Status = "Paid";
                    Console.WriteLine("Order Status: " + "\t" + order1.Status);
                    Console.WriteLine();
                }
                else if (t is SeniorCitizen)       // downcast to SeniorCitizen Object
                {
                    // 9a) prompt user for a response depending on the type of ticket ordered:
                    int yob = YOBValidation("Please enter year of birth (senior citizen)");

                    // Type casting 
                    SeniorCitizen sc = (SeniorCitizen)t;
                    // 9b) create a Ticket object (SeniorCitizien) with the information given
                    Ticket ti2 = new SeniorCitizen(s1, yob);
                    sc.YearOfBirth = yob;
                    // 9c) add the ticket object to the ticket list of the order
                    order1.AddTicket(ti2);
                    // 9d) update seats remaining for the movie screening 
                    s1.SeatsRemaining = 50;
                    s1.SeatsRemaining -= ticketQty;
                    Console.WriteLine("Seats Remaining: " + s1.SeatsRemaining);
                    // 10) list amount payable
                    //double price = sc.CalculatePrice();
                    // 11) prompt user to press any key to make payment 
                    Console.WriteLine("Please press any key to make payment");
                    Console.ReadKey();
                    Console.WriteLine();
                    // 12) fill in the amount details to the new order
                    order1.Amount = 6 * ticketQty;
                    Console.WriteLine("Amount Paid: ($)" + "\t" + order1.Amount);
                    // 13) change order status to "Paid"
                    order1.Status = "Paid";
                    Console.WriteLine("Order Status: " + "\t" + order1.Status);
                    Console.WriteLine();
                }
                else if (t is Adult)     // downcast to Adult Object
                {
                    // 9a) prompt user for a response depending on the type of ticket ordered:
                    string cn = RetrieveString("Do you want to purchase popcorn for $3 [Y/N] - Adult");
                    // Type casting 
                    Adult a = (Adult)t;

                    if (cn == "Y")
                    {
                        a.PopcornOffer = true;
                        // 9b) create a Ticket object (Adult) with the information given
                        Ticket ti3 = new Adult(s1, true);
                        // 9c) add the ticket object ticket3 to the ticket list of the order
                        order1.AddTicket(ti3);
                        // 9d) update seats remaining for the movie screening
                        s1.SeatsRemaining = 50;
                        s1.SeatsRemaining -= ticketQty;
                        Console.WriteLine("Seats Remaining: " + s1.SeatsRemaining);
                        // 10) list amount payable
                        double Cost = a.CalculatePrice() + 3.00;
                        Console.WriteLine(Cost);
                        // 11) prompt user to press any key to make payment
                        Console.WriteLine("Please press any key to make payment");
                        Console.ReadKey();
                        Console.WriteLine();
                        // 12) fill in the amount details to the new order
                        order1.Amount = 14 * ticketQty;
                        Console.WriteLine("Amount Paid: ($)" + "\t" + order1.Amount);
                        // 13) change order status to "Paid"
                        order1.Status = "Paid";
                        Console.WriteLine("Order Status: " + "\t" + order1.Status);
                        Console.WriteLine();
                    }
                    else
                    {
                        a.PopcornOffer = false;
                        // 9b) create a Ticket object (Adult) with the information given
                        Ticket ti4 = new Adult(s1, false);
                        // 9c) add the ticket object ticket 4 to ticketList
                        order1.AddTicket(ti4);
                        // 9d) update seats remaining for the movie screening
                        s1.SeatsRemaining = 50;
                        s1.SeatsRemaining -= ticketQty;
                        Console.WriteLine("Seats Remaining: " + s1.SeatsRemaining);
                        //10) list amount payable
                        // double Price = a.CalculatePrice();
                        // 11) prompt user to press any key to make payment
                        Console.WriteLine("Please press any key to make payment");
                        Console.ReadKey();
                        Console.WriteLine();
                        // 12) fill in the amount details to the new order
                        order1.Amount = 11 * ticketQty;
                        Console.WriteLine("Amount Paid: ($)" + "\t" + order1.Amount);
                        // 13) change order status to "Paid"
                        order1.Status = "Paid";
                        Console.WriteLine("Order Status: " + "\t" + order1.Status);
                        Console.WriteLine();
                    }
                }
            }
            order1.TicketList = ticketList;
            return order1;
        }

        public static Order CancelMovieTicket(List<Order> orderList, List<Screening> ScreeningList, Order order1, Screening s1, List<Ticket> ticketList)
        {
            // 1) prompt user for order number
            int c = GetInt("Please enter your Order Number:");
            if (c < 0)
            {
                Console.WriteLine("Please enter a valid Order number starting from 1");
            }
            // Find the index of the order number entered by user.
            // 2) retrieve the selected order
            orderList.FindIndex(x => x.OrderNo == c);

            foreach (Order o in orderList)
            {
                int OrderNo = o.OrderNo;
                Console.WriteLine(o);
            }

            // 3)  check if the screening in the selected order is screened

            foreach (Screening s in ScreeningList)
            {
                if (DateTime.Now > s.ScreeningDateTime)
                {
                    Console.WriteLine("Movie screening has started. Cancellation is not allowed.");
                }
                else
                {
                    Console.WriteLine("Movie screening has not started. Cancellation is allowed");
                }
            }

            // 4) update seat remaining for the movie screening based on the selected order
            s1.SeatsRemaining = 15;
            s1.SeatsRemaining += order1.TicketList.Count;
            Console.WriteLine("Seats Remaining: " + "\t" + s1.SeatsRemaining);

            // 5) change order status to “Cancelled”
            order1.Status = "Cancelled";
            Console.WriteLine("Order Status: " + "\t" + order1.Status);

            // 6)  display a message indicating that the amount is refunded
            Console.WriteLine("The amount has been refunded.");

            // 7) display the status of the cancelation (i.e. successful or unsuccessful)
            Console.WriteLine("Status of cancellation:  " + "Successful");

            return order1;

        }

        // initialize the ticket list.
        static void InitTicketList(List<Ticket> ticketList)
        {
            Screening s1 = new Screening();
            Ticket t1 = new Student(s1, "Primary");
            Ticket t2 = new SeniorCitizen(s1, 1965);
            Ticket t3 = new Adult(s1, true);
            ticketList.Add(t1);
            ticketList.Add(t2);
            ticketList.Add(t3);
        }

        // string data type input validations
        static string RetrieveString(string prompt)
        {
            string c;
            string cn;
            while (true)
            {
                try
                {
                    Console.Write(prompt);
                    c = Console.ReadLine();
                    cn = Console.ReadLine();
                    break;
                }

                catch (Exception)
                {
                    Console.WriteLine("Invalid input! Please try again.");
                }
            }
            return c;
        }

        // DateTime data input validations
        static DateTime GetDateTime(string prompt)
        {
            DateTime dt;
            while (true)
            {
                try
                {
                    Console.Write(prompt);
                    dt = Convert.ToDateTime(Console.ReadLine());
                    break;
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Invalid Date! Please try again.");
                }
            }
            return dt;
        }
        // Integer data input validations
        static int GetInt(string prompt)
        {
            int ticketQty;
            while (true)
            {
                try
                {
                    Console.Write(prompt);
                    ticketQty = Convert.ToInt32(Console.ReadLine());
                    break;
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Invalid number! Please try again.");
                }
            }
            return ticketQty;
        }

        // Senior citizen year of birth validations
        static int YOBValidation(string prompt)
        {
            int yob;
            while (true)
            {
                try
                {
                    Console.WriteLine(prompt);
                    yob = Convert.ToInt32(Console.ReadLine());
                    break;
                }

                catch (FormatException)
                {
                    Console.WriteLine("Input not in correct format ");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid Year of birth! Please try again.");
                }
            }
            return yob;
        }

        // menu validations  
        static int MenuValidation(string prompt)
        {
            int opt;
            while (true)
            {
                try
                {
                    Console.Write(prompt);
                    opt = Convert.ToInt32(Console.ReadLine());
                    break;
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Invalid option! Please try again with valid option.");
                }
            }
            return opt;
        }
    }
}
//




