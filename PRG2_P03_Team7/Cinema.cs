﻿// ========================================== 
// Student Number: S10223203, S10228079
// Student Name: Chong Jerome, Ho Min Teck
// Module Group: P03
// ==========================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_P03_Team7
{
    class Cinema
    {
        public string Name { get; set; }
        public int HallNo { get; set; }
        public int Capacity { get; set; }

        public Cinema() { }
        
        public Cinema(string n, int hn, int c) { Name = n; HallNo = hn; Capacity = c; }

        public override string ToString()
        {
            return Name + "\t" + HallNo + "\t" + Capacity;
        }
    }
}
