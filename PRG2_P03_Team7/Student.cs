﻿/// ========================================== 
// Student Number: S10223203, S10228079
// Student Name: Chong Jerome, Ho Min Teck
// Module Group: P03
// ==========================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_P03_Team7
{
    class Student:Ticket
    {
        public Screening Screening { get; set; }
        public string LevelOfStudy { get; set; }
        public DayOfWeek DayOfWeek { get; set; }

        public  Student() {  }

        public Student(Screening s,  string l)
        {
            Screening = s;
            LevelOfStudy = l;
        }

        public override double CalculatePrice()
        {
            double price = 0.00;
            int dow = (int) Screening.ScreeningDateTime.DayOfWeek;
            int numberofdays = (Screening.ScreeningDateTime - Screening.Movie.OpeningDate).Days;
            if (Screening.ScreeningType == "2D") // 2D Movie
            {
                if (dow == 5 || dow == 6 || dow == 0) // Friday - Sunday 
                    price = 12.50;
                else if (numberofdays <= 7)     // First 7 days of movie screening date 
                    price = 8.50;
                else
                    price = 7.00;             // Monday - Thursday
            }
           
            else                                    // 3D Movie
            {
                if (dow == 5 || dow == 6 || dow == 0)   // Friday - Sunday
                    price = 14.00;
                else if (numberofdays <= 7)     // First 7 days
                    price = 11.00;
                else
                    price = 8.00;      // Monday - Thursday
            }
            return price;
        }
        

        public override string ToString()
        {
            return Screening + "\t" + LevelOfStudy;
        }
    }
}
