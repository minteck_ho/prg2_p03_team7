﻿// ========================================== 
// Student Number: S10223203, S10228079
// Student Name: Chong Jerome, Ho Min Teck
// Module Group: P03
// ==========================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_P03_Team7
{
    class Order
    {
        public int OrderNo { get; set; }
        public DateTime OrderDateTime { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public List<Ticket> TicketList { get; set; } = new List<Ticket>();

        public Order() { }
        public Order(int orderno, DateTime orddatetime, string status) { OrderNo = orderno; OrderDateTime = orddatetime; Status = status; }

        public void AddTicket(Ticket t)
        { TicketList.Add(t); }

        public override string ToString()
        {
            return base.ToString() + OrderNo + OrderDateTime + Amount + Status;
        }
    }
}
