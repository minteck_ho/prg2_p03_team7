﻿// ========================================== 
// Student Number: S10223203, S10228079
// Student Name: Chong Jerome, Ho Min Teck
// Module Group: P03
// ==========================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_P03_Team7
{
    abstract class Ticket : Screening
    {
        public Screening Screening { get; set; }

        public Ticket() { }

        public Ticket(Screening s) { Screening = s; }

        public abstract double CalculatePrice();

        public override string ToString()
        {
            return base.ToString() + Screening;
        }
    }
}
